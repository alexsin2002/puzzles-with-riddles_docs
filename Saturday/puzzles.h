//
//  puzzles.h
//  Saturday
//
//  Created by Ученик on 25.11.17.
//  Copyright © 2017 Синицын Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface puzzles : NSObject

@property (nonatomic,strong) UIImage * pictures;
@property (nonatomic,strong) NSString * question;
@property (nonatomic, strong) NSString * answer;


@end
