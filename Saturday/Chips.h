//
//  Chips.h
//  Saturday
//
//  Created by Ученик on 25.11.17.
//  Copyright © 2017 Синицын Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Chips : NSObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic) int presentValue;
@property (nonatomic, strong) UIImage * chipImage;
@property (nonatomic) int changeInPosition;


@end
