//
//  ViewController.m
//  Game
//
//  Created by Ученик on 16.11.17.
//  Copyright © 2017 Синицын Алексей. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic,weak)IBOutlet UIButton * fightButton;

@property (nonatomic,weak)IBOutlet UITextField * forceTextField_1;
@property (nonatomic,weak)IBOutlet UITextField * defenseTextField_1;
@property (nonatomic,weak)IBOutlet UITextField * forceTextField_2;
@property (nonatomic,weak)IBOutlet UITextField * defenseTextField_2;

@property (nonatomic,weak)IBOutlet UITextField * nameTextField_1;
@property (nonatomic,weak)IBOutlet UITextField * nameTextField_2;

@end

@implementation  ViewController

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
     return YES;
}

-(IBAction)fightButton_pressed
{
    int force_1 = self.forceTextField_1.text.intValue;
    int defense_1 = self.forceTextField_1.text.intValue;
    int force_2 = self.forceTextField_2.text.intValue;
    int defense_2 = self.forceTextField_2.text.intValue;
    
    int result_1 = defense_1 - force_2;
    int result_2 = defense_2 - force_1;
    
    NSString *name1 = self.nameTextField_1.text;
    NSString *name2 = self.nameTextField_2.text;
    
  
    
    if(result_1 == result_2) {
        
        [self fightFinishedWithWinner:@"никто"];
    } else if (result_1 > result_2) {

        [self fightFinishedWithWinner:name1];
    } else {
        [self fightFinishedWithWinner:name2];
    }
    
    
}

-(void) fightFinishedWithWinner:(NSString *) name
{
    NSString *message = [NSString stringWithFormat:@"Победил %@",name];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"Закрыть" otherButtonTitles:nil];
    [alertView show];
}

@end